package main

import (
	"flag"
	"log"
	"time"

	pb "gitlab.com/weepi/auth/proto"

	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

var (
	endpointFlag = flag.String("endpoint", "127.0.0.1:1665", "Endpoint to listen to")
	uuidFlag     = flag.String("uuid", "696638b6-2179-4cfc-8a1f-61cdfec57cbe", "User UUID")
)

func init() {
	flag.Parse()
}

func main() {
	// Establish conn with the server
	conn, err := grpc.Dial(*endpointFlag, grpc.WithInsecure())
	if err != nil {
		log.Fatal("did not connect")
	}
	defer conn.Close()

	// New weepi account client
	client := pb.NewWeepiAuthClient(conn)

	// Prepare account creation infos
	creds := &pb.Creds{
		Uuid: &pb.Uuid{
			Value: *uuidFlag,
		},
		Secret: &pb.Secret{
			Value: "Passw0rd_",
		},
		AuthType: pb.AuthType(0),
	}

	// If the insertion take more than 2 seconds, cancel it
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	// Ask to insert creds
	ret, err := client.GetToken(ctx, creds)
	if err != nil {
		log.Fatalf("Error inserting creds : %v", err)
	}

	log.Printf("Token: %s", ret.String())
}
