package main

import (
	"flag"
	"log"
	"time"

	pb "gitlab.com/weepi/auth/proto"

	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

var (
	endpoint  = flag.String("endpoint", "127.0.0.1:1664", "Endpoint to listen to")
	tokenFlag = flag.String("token", "", "User token")
	uuidFlag  = flag.String("uuid", "", "User UUID")
)

func init() {
	flag.Parse()
}

func main() {
	// Establish conn with the server
	conn, err := grpc.Dial(*endpoint, grpc.WithInsecure())
	if err != nil {
		log.Fatal("did not connect")
	}
	defer conn.Close()

	// New weepi account client
	client := pb.NewWeepiAuthClient(conn)

	// If the insertion take more than 2 seconds, cancel it
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	md := metadata.Pairs("authorization", *tokenFlag)
	ctx = metadata.NewOutgoingContext(ctx, md)

	// Ask to delete creds
	_, err = client.UpdateCreds(ctx, &pb.Creds{
		Uuid: &pb.Uuid{
			Value: *uuidFlag,
		},
		Password: &pb.Password{
			Value: "Aw_esom3",
		},
	})
	if err != nil {
		log.Fatalf("Error updating creds : %v", err)
	}

	// creds inserted!
	log.Printf("Creds updated!")
}
