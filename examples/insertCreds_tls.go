package main

import (
	"crypto/tls"
	"crypto/x509"
	"flag"
	"io/ioutil"
	"log"
	"time"

	pb "gitlab.com/weepi/auth/proto"

	"github.com/satori/go.uuid"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/metadata"
)

const (
	client_crt = "../certs/client/weepi_auth.crt"
	client_key = "../certs/client/weepi_auth.key"
	ca_crt     = "../certs/ca/ca.crt"
)

var (
	endpoint = flag.String("endpoint", "127.0.0.1:1664", "Endpoint to listen to")
)

func init() {
	flag.Parse()
}

func getTLSCreds() (*credentials.TransportCredentials, error) {
	// Load the client certificates from disk
	certificate, err := tls.LoadX509KeyPair(client_crt, client_key)
	if err != nil {
		return nil, err
	}

	// Create a certificate pool from the certificate authority
	certPool := x509.NewCertPool()
	ca, err := ioutil.ReadFile(ca_crt)
	if err != nil {
		return nil, err
	}

	// Append the certificates from the CA
	if ok := certPool.AppendCertsFromPEM(ca); !ok {
		return nil, err
	}

	creds := credentials.NewTLS(&tls.Config{
		ServerName:   "localhost", // NOTE: this is required!
		Certificates: []tls.Certificate{certificate},
		RootCAs:      certPool,
	})

	return &creds, nil
}

func main() {
	tlsCreds, err := getTLSCreds()
	if err != nil {
		panic(err)
	}

	// Establish conn with the server
	conn, err := grpc.Dial(*endpoint,
		grpc.WithTransportCredentials(*tlsCreds),
	)
	if err != nil {
		log.Fatal("did not connect")
	}
	defer conn.Close()

	// New weepi account client
	client := pb.NewWeepiAuthClient(conn)

	u := uuid.NewV4()
	log.Printf("Generated UUID: %v\n", u)
	// Prepare account creation infos
	creds := &pb.Creds{
		Uuid: &pb.Uuid{
			Value: u.String(),
		},
		Secret: &pb.Secret{
			Value: "_th2Password",
		},
		AuthType: pb.AuthType_STANDARD,
	}

	// If the insertion take more than 2 seconds, cancel it
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	md := metadata.Pairs("useruuid", uuid.Nil.String())
	ctx = metadata.NewOutgoingContext(ctx, md)

	// Ask to insert creds
	_, err = client.InsertCreds(ctx, creds)
	if err != nil {
		log.Fatalf("Error inserting creds : %v", err)
	}

	// creds inserted!
	log.Printf("Creds inserted!")
}
