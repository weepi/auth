package main

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"strings"
	"unicode"

	pb "gitlab.com/weepi/auth/proto"
	"gitlab.com/weepi/auth/schemes"
	"gitlab.com/weepi/auth/token"
	"gitlab.com/weepi/common/db"
	"gitlab.com/weepi/common/utils"

	"github.com/futurenda/google-auth-id-token-verifier"
	"github.com/golang/protobuf/ptypes/empty"
	fb "github.com/huandu/facebook"
	"github.com/nats-io/go-nats"
	"github.com/satori/go.uuid"
	"golang.org/x/crypto/bcrypt"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
)

const (
	MinPasswordLength  = 8
	MaxPasswordLength  = 128
	HeaderAuthorize    = "authorization"
	TestGoogleToken    = "510814972226-mr8kf7rumf0sq05clckijralgasu04ua.apps.googleusercontent.com"
	AndroidGoogleToken = "157222842768-geu66kvk3hsk0nfonojlrf99baq5mb90.apps.googleusercontent.com"
	IosGoogleToken     = "157222842768-nhvn4a2e796l2vk90n0oloe0vmogjgob.apps.googleusercontent.com"
)

var (
	ErrPasswordMinLengthPolicy = fmt.Errorf("password length must be at least %d characters", MinPasswordLength)
	ErrPasswordMaxLengthPolicy = fmt.Errorf("password length must be less than %d characters", MaxPasswordLength)
	ErrPasswordNumericPolicy   = errors.New("password must contain at least a numeric value")
	ErrPasswordSpecialPolicy   = errors.New("password must contain at least a special character")
	ErrProcessingPassword      = errors.New("error processing password")
	ErrDatabase                = errors.New("database error")
	ErrUserNotFound            = errors.New("user not found")
	ErrInternal                = errors.New("internal error")
	ErrWrongPassword           = errors.New("wrong password")
	ErrInvalidUUID             = errors.New("invalid uuid")
	ErrTokenNotFound           = errors.New("token not found")
	ErrWrongAuthTypeSet        = errors.New("wrong authentication type")

	NumericRangeTable = []*unicode.RangeTable{unicode.Number, unicode.Digit}
	SpecialRangeTable = []*unicode.RangeTable{unicode.Space, unicode.Symbol, unicode.Punct, unicode.Mark}
)

type WeepiAuth struct {
	db  *db.DB
	msg *nats.EncodedConn
}

func (wa *WeepiAuth) registerMsgActions() error {
	ctx := context.Background()

	// Auth insert
	if _, err := wa.msg.Subscribe("auth.insert", func(subj, reply string, creds *pb.Creds) {
		Logger.Debugf("I saw a message in auth.insert: '%v'", creds)
		// insert cred
		_, errb := wa.InsertCreds(ctx, creds)
		var errMsg utils.Error
		if errb != nil {
			errMsg.Msg = errb.Error()
		}
		if err := wa.msg.Publish(reply, &errMsg); err != nil {
			Logger.With("error", err).Error("publishing error")
		}
	}); err != nil {
		Logger.With("error", err).Error("auth insert subscribing failure")
		return err
	}

	// Auth check
	if _, err := wa.msg.Subscribe("auth.check", func(subj, reply string, ts string) {
		Logger.Debugf("I saw a message in auth.check: '%v'", ts)
		// check auth
		auth, errb := wa.isAuth(ts)
		if errb != nil {
			Logger.With("error", errb).Error("auth failure")
		}
		if err := wa.msg.Publish(reply, &auth); err != nil {
			Logger.With("error", err).Error("publishing error")
		}
	}); err != nil {
		Logger.With("error", err).Error("auth check subscribing failure")
		return err
	}

	// Auth delete
	if _, err := wa.msg.Subscribe("auth.delete", func(subj, reply string, u uuid.UUID) {
		Logger.Debugf("I saw a message in auth.delete: '%s'", u.String())
		errb := wa.deleteCreds(u)
		if errb != nil {
			Logger.With("error", errb).Error("could not delete creds")
		}
		if err := wa.msg.Publish(reply, &errb); err != nil {
			Logger.With("error", err).Error("publishing error")
		}
	}); err != nil {
		Logger.With("error", err).Error("auth delete subscribing failure")
		return err
	}

	// Auth update
	if _, err := wa.msg.Subscribe("auth.update", func(subj, reply string, creds *pb.Creds) {
		Logger.Debugf("I saw a message in auth.update: '%s'", creds)
		_, errb := wa.UpdateCreds(ctx, creds)
		if errb != nil {
			Logger.With("error", errb).Error("could not update creds")
		}
		if err := wa.msg.Publish(reply, &errb); err != nil {
			Logger.With("error", err).Error("publishing error")
		}
	}); err != nil {
		Logger.With("error", err).Error("auth update subscribing failure")
		return err
	}

	return nil
}

func NewWeepiAuth() (*WeepiAuth, error) {
	// Db conn
	Logger.Infof("Attempting to connect to the database at %s...", Cfg.DBHost)
	d, err := db.New(&db.Config{
		Driver:   Cfg.DBDriver,
		Host:     Cfg.DBHost,
		User:     Cfg.DBUser,
		Name:     Cfg.DBName,
		SSLMode:  Cfg.DBSSLMode,
		Password: Cfg.DBPassword,
	}, Logger)
	if err != nil {
		return nil, err
	}
	d.LogMode(true)

	// Migrate the schema
	if err = d.AutoMigrate(&schemes.Creds{}).Error; err != nil {
		return nil, err
	}
	if err = d.AutoMigrate(&schemes.Token{}).Error; err != nil {
		return nil, err
	}

	// NATS conn
	dsn := fmt.Sprintf("nats://%s", Cfg.NATSAddr)
	nc, err := nats.Connect(dsn)
	if err != nil {
		Logger.With("error", err).Fatal("can't connect to NATS")
	}
	econn, err := nats.NewEncodedConn(nc, nats.JSON_ENCODER)
	if err != nil {
		Logger.With("error", err).Fatal("can't create encoded conn")
	}

	return &WeepiAuth{
		db:  d,
		msg: econn,
	}, nil
}

func checkPasswordPolicy(password string) error {
	if len(password) < MinPasswordLength {
		return ErrPasswordMinLengthPolicy
	}
	if len(password) > MaxPasswordLength {
		return ErrPasswordMaxLengthPolicy
	}

	var numericFlag bool
	var specialFlag bool

	for _, c := range password {
		if !numericFlag && unicode.IsOneOf(NumericRangeTable, c) {
			numericFlag = true
		}
		if !specialFlag && unicode.IsOneOf(SpecialRangeTable, c) {
			specialFlag = true
		}
	}

	if !numericFlag {
		return ErrPasswordNumericPolicy
	}
	if !specialFlag {
		return ErrPasswordSpecialPolicy
	}

	return nil
}

func checkGoogleToken(token string) error {
	v := googleAuthIDTokenVerifier.Verifier{}
	err := v.VerifyIDToken(token, []string{TestGoogleToken})
	if err == nil {
		return nil
	}
	err = v.VerifyIDToken(token, []string{AndroidGoogleToken})
	if err == nil {
		return nil
	}
	err = v.VerifyIDToken(token, []string{IosGoogleToken})
	if err != nil {
		return errors.New("Wrong google token")
	}
	return nil
}

func decodeGoogleIDToken(idToken string) (gplusID string, err error) {
	var set struct {
		Sub string
	}
	if idToken != "" {
		// Check that the padding is correct for a base64decode
		parts := strings.Split(idToken, ".")
		if len(parts) < 2 {
			return "", errors.New("Malformed ID token")
		}
		// Decode the ID token
		s := parts[1]
		switch len(s) % 4 {
		case 2:
			s += "=="
		case 3:
			s += "="
		}

		b, err := base64.URLEncoding.DecodeString(s)
		if err != nil {
			return "", fmt.Errorf("Malformed ID token: %v", err)
		}
		err = json.Unmarshal(b, &set)
		if err != nil {
			return "", fmt.Errorf("Malformed ID token: %v", err)
		}
	}
	return set.Sub, nil
}

func getFacebookId(token string) (string, error) {
	res, err := fb.Get("/me", fb.Params{
		"fields":       "id",
		"access_token": token,
	})

	if err != nil {
		return "", errors.New("Wrong facebook token")
	}

	return res["id"].(string), nil
}

func getGoogleId(token string) (string, error) {
	err := checkGoogleToken(token)
	if err != nil {
		return "", err
	}
	return decodeGoogleIDToken(token)
}

func (s *WeepiAuth) InsertCreds(ctx context.Context, req *pb.Creds) (*empty.Empty, error) {
	var err error
	var pass []byte
	var strPass string

	switch req.GetAuthType() {
	case pb.AuthType_STANDARD:
		err = checkPasswordPolicy(req.GetSecret().GetValue())
		if err != nil {
			Logger.With("error", err).Error("Password does not respect policy")
			break
		}

		pass, err = bcrypt.GenerateFromPassword([]byte(req.GetSecret().GetValue()), bcrypt.DefaultCost)
		if err != nil {
			Logger.With("error", err).Error("bcrypt hashing error")
			err = ErrProcessingPassword
		}
	case pb.AuthType_FACEBOOK:
		strPass, err = getFacebookId(req.GetSecret().GetValue())
		pass = []byte(strPass)
	case pb.AuthType_GOOGLE:
		strPass, err = getGoogleId(req.GetSecret().GetValue())
		pass = []byte(strPass)
	default:
		err = ErrWrongAuthTypeSet
	}
	if err != nil {
		return nil, err
	}

	u, err := uuid.FromString(req.GetUuid().GetValue())
	if err != nil {
		Logger.With("error", err).Error("UUID to string error")
		return nil, ErrProcessingPassword
	}
	if err = s.db.Create(&schemes.Creds{
		UUID:      u,
		HPassword: pass,
		AuthType:  pb.AuthType_name[int32(req.GetAuthType())],
	}).Error; err != nil {
		Logger.With("error", err).Error("inserting password bhash error")
		return nil, ErrProcessingPassword
	}

	return &empty.Empty{}, nil
}

func (s *WeepiAuth) getStandardToken(req *pb.Creds) (schemes.Creds, error) {

	var creds schemes.Creds

	// check valid UUID in req
	if _, err := uuid.FromString(req.GetUuid().GetValue()); err != nil {
		Logger.With("error", err).Debug(ErrInvalidUUID.Error)
		return creds, ErrInvalidUUID
	}

	// check if user exists
	if s.db.Where("uuid = ? AND auth_type = ?", req.GetUuid().GetValue(), pb.AuthType_name[int32(req.GetAuthType())]).First(&creds).RecordNotFound() {
		Logger.Debugf("user with uuid %s and auth_type %s not found", req.GetUuid().GetValue(), req.GetAuthType().String())
		return creds, ErrUserNotFound
	}
	if err := s.db.Error; err != nil {
		Logger.With("error", err).Errorf("quering creds for %s error", req.GetUuid().GetValue())
		return creds, ErrDatabase
	}

	// check password
	fmt.Printf("%+v\n", creds)
	if err := bcrypt.CompareHashAndPassword(creds.HPassword, []byte(req.GetSecret().GetValue())); err != nil {
		if err == bcrypt.ErrMismatchedHashAndPassword {
			Logger.With("error", err).Debug("wrong password")
			return creds, ErrWrongPassword
		}
		Logger.With("error", err).Error("check passwords error")
		return creds, ErrInternal
	}

	return creds, nil
}

func (s *WeepiAuth) getOAuthToken(req *pb.Creds) (schemes.Creds, error) {
	var id string
	var err error = nil
	var creds schemes.Creds

	if req.GetAuthType() == pb.AuthType_FACEBOOK {
		id, err = getFacebookId(req.GetSecret().GetValue())
	} else if req.GetAuthType() == pb.AuthType_GOOGLE {
		id, err = getGoogleId(req.GetSecret().GetValue())
	}
	if err != nil {
		return creds, err
	}

	if s.db.Where("h_password = ? AND auth_type = ?", id, pb.AuthType_name[int32(req.GetAuthType())]).First(&creds).RecordNotFound() {
		Logger.Debugf("user with auth_type %s not found", req.GetAuthType().String())
		return creds, ErrUserNotFound
	}
	if err := s.db.Error; err != nil {
		Logger.With("error", err).Errorf("quering creds for %s error", req.GetUuid().GetValue())
		return creds, ErrDatabase
	}
	return creds, nil
}

func (s *WeepiAuth) GetToken(ctx context.Context, req *pb.Creds) (*pb.Token, error) {
	var err error = nil
	var creds schemes.Creds

	if req.GetAuthType() == pb.AuthType_STANDARD {
		creds, err = s.getStandardToken(req)
	} else {
		creds, err = s.getOAuthToken(req)
	}
	if err != nil {
		return nil, err
	}

	// TODO: Check if already have a valid token before new gen.
	// Limit at 1 token per user for now

	// generate new token
	tk, err := token.New()
	if err != nil {
		Logger.With("error", err).Error("token generation error")
		return nil, ErrInternal
	}

	// insert token in DB
	if err = s.db.Model(&creds).Association("Tokens").Append(schemes.Token{Value: tk.String()}).Error; err != nil {
		Logger.With("error", err).Error("insert token error")
		return nil, ErrInternal
	}

	return &pb.Token{Value: tk.String()}, nil
}

func (s *WeepiAuth) AuthInterceptor(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
	// Non authenticated methods
	switch info.FullMethod {
	case "/WeepiAuth/GetToken", "/WeepiAuth/IsAuth":
		return handler(ctx, req)
	default:
	}

	// check user authenticated
	auth, err := s.IsAuth(ctx, nil)
	if err != nil {
		return nil, err
	}

	Logger.Debugf("uuid '%s' set in meta", auth.GetUuid().GetValue())
	// set user UUID in outgoing ctx
	md := metadata.Pairs("useruuid", auth.GetUuid().GetValue())
	ctx = metadata.NewOutgoingContext(ctx, md)
	ctx = metadata.NewIncomingContext(ctx, md)

	return handler(ctx, req)
}

func (s *WeepiAuth) getUUID(inputToken *token.Token) (*uuid.UUID, error) {
	var token schemes.Token
	if s.db.Where("value = ?", inputToken.String()).First(&token).RecordNotFound() {
		Logger.Debugf("could not find token '%s' in the database", inputToken.String())
		return nil, ErrTokenNotFound
	}
	if s.db.Error != nil {
		return nil, ErrInternal
	}
	return &token.CredsUUID, nil
}

func getFromMeta(ctx context.Context, key string) (string, error) {
	meta, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return "", errors.New("missing context metadata")
	}
	vv, ok := meta[key]
	if !ok {
		return "", fmt.Errorf("could not find key '%s' in meta", key)
	}
	return vv[0], nil
}

func (s *WeepiAuth) isAuth(ts string) (*pb.Auth, error) {
	t, err := token.FromString(ts)
	if err != nil {
		return nil, err
	}

	u, err := s.getUUID(t)
	if err != nil {
		return nil, err
	}

	return &pb.Auth{
		IsAuth: err == nil,
		Uuid: &pb.Uuid{
			Value: u.String(),
		}}, nil
}

func (s *WeepiAuth) IsAuth(ctx context.Context, _ *empty.Empty) (*pb.Auth, error) {
	// Extract auth
	reqToken, err := getFromMeta(ctx, "authorization")
	if err != nil {
		return nil, grpc.Errorf(codes.Unauthenticated, err.Error())
	}

	a, err := s.isAuth(reqToken)
	if err != nil {
		Logger.With("error", err).Error("auth failure")
		return nil, grpc.Errorf(codes.Unauthenticated, "auth failure")
	}

	Logger.Debugf("auth ok with token '%s', UUID: '%s'", reqToken, a.GetUuid().String())

	return a, nil
}
