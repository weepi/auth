package token

import (
	"crypto/rand"
	"encoding/base64"
)

const (
	TokenLength = 32
)

type Token struct {
	value []byte
}

func New() (*Token, error) {

	b := make([]byte, TokenLength)
	if _, err := rand.Read(b); err != nil {
		return nil, err
	}

	return &Token{
		value: b,
	}, nil
}

func (t *Token) String() string {
	return base64.StdEncoding.EncodeToString(t.value)
}

func FromString(t string) (*Token, error) {
	d, err := base64.StdEncoding.DecodeString(t)
	if err != nil {
		return nil, err
	}
	return &Token{value: d}, nil
}

func (t *Token) Bytes() []byte {
	return []byte(t.String())
}

func FromBytes(b []byte) (*Token, error) {
	e, err := FromString(string(b))
	if err != nil {
		return nil, err
	}
	return e, nil
}
