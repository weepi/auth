FROM golang:alpine

MAINTAINER Bastien Dhiver <contact@bastn.fr>

ENV APP_PATH=$GOPATH/src/gitlab.com/weepi/auth

ADD . $APP_PATH
WORKDIR $APP_PATH

RUN apk update
RUN apk add make git

RUN make install

ENTRYPOINT $GOPATH/bin/auth
