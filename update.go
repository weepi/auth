package main

import (
	pb "gitlab.com/weepi/auth/proto"

	"github.com/golang/protobuf/ptypes/empty"
	"golang.org/x/net/context"
)

func (s *WeepiAuth) UpdateCreds(ctx context.Context, req *pb.Creds) (*empty.Empty, error) {
	if _, err := s.DeleteCreds(ctx, &empty.Empty{}); err != nil {
		Logger.With("error", err).Error("could not remove old creds")
		return nil, err
	}

	if _, err := s.InsertCreds(ctx, req); err != nil {
		Logger.With("error", err).Error("could not insert new creds")
		return nil, err
	}
	Logger.Debugf("Creds updated for UUID '%s'", req.GetUuid().String())
	return &empty.Empty{}, nil
}
