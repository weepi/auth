package main

import (
	"gitlab.com/weepi/auth/schemes"

	"github.com/golang/protobuf/ptypes/empty"
	"github.com/satori/go.uuid"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
)

func (s *WeepiAuth) deleteCreds(u uuid.UUID) error {
	// remove tokens
	if err := s.db.Where("creds_uuid = ?", u.String()).Delete(&schemes.Token{}).Error; err != nil {
		return err
	}

	// remove creds
	if err := s.db.Where("uuid = ?", u.String()).Delete(&schemes.Creds{}).Error; err != nil {
		return err
	}

	return nil
}

func (s *WeepiAuth) DeleteCreds(ctx context.Context, _ *empty.Empty) (*empty.Empty, error) {

	// Get user uuid from meta
	meta, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return nil, grpc.Errorf(codes.Unauthenticated, "missing context metadata")
	}
	if len(meta["useruuid"]) != 1 {
		Logger.Debug("could not find user uuid in meta")
		return nil, grpc.Errorf(codes.Unauthenticated, "invalid user uuid")
	}

	u, err := uuid.FromString(meta["useruuid"][0])
	if err != nil {
		Logger.With("error", err).Errorf("could not process uuid '%s'", meta["useruuid"][0])
		return nil, grpc.Errorf(codes.Unauthenticated, "invalid user uuid")
	}

	if err := s.deleteCreds(u); err != nil {
		Logger.With("error", err).Error("db error")
		return nil, grpc.Errorf(codes.Internal, "internal error")
	}

	Logger.Debugf("Creds deleted for UUID: '%s'", u.String())
	return &empty.Empty{}, nil
}
