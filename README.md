# Weepi auth !

The weepi auth microservice was used by [Weepen](https://weepen.io/) to manage user authentication.

The docker compose file in the [deploy repository](https://gitlab.com/weepi/deploy) was used to manage it.

