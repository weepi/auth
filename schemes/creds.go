package schemes

import (
	"time"

	"github.com/satori/go.uuid"
)

type Creds struct {
	UUID      uuid.UUID `gorm:"primary_key;type:uuid"`
	CreatedAt time.Time
	UpdatedAt time.Time
	HPassword []byte `gorm:"not null"`
	Tokens    []Token
	AuthType  string `gorm:"not null;type:auth_type"`
}

type Token struct {
	ID        uint `gorm:"primary_key"`
	CreatedAt time.Time
	UpdatedAt time.Time
	Value     string    `gorm:"not null;unique"`
	CredsUUID uuid.UUID `gorm:index`
}
