package main

import (
	"flag"
	"fmt"
	"net"
	"os"
	"time"

	"github.com/caarlos0/env"
	"github.com/joho/godotenv"
	pb "gitlab.com/weepi/auth/proto"
	"gitlab.com/weepi/common/log"
	"gitlab.com/weepi/common/utils"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

type config struct {
	ListenAddr string `env:"LISTEN_ADDR,required"`
	DBDriver   string `env:"DB_DRIVER,required"`
	DBHost     string `env:"DB_HOST,required"`
	DBUser     string `env:"DB_USER,required"`
	DBName     string `env:"DB_NAME,required"`
	DBSSLMode  string `env:"DB_SSLMODE" envDefault:"disable"`
	DBPassword string `env:"DB_PASSWORD"`
	NATSAddr   string `env:"NATS_ADDR"`
	UseTLS     bool   `env:"USE_TLS" envDefault:"true"`
	TLSCrt     string `env:"TLS_CRT"`
	TLSKey     string `env:"TLS_KEY"`
}

const (
	configFilename = "config"
	tagName        = "env"
)

var (
	Logger *log.Logger
	Cfg    config
)

func init() {
	var helpFlag = flag.Bool("help", false, "display help")
	flag.Parse()
	if *helpFlag {
		help()
		os.Exit(0)
	}

	var err error
	Logger, err = log.New()
	if err != nil {
		panic(err)
	}
}

func help() {
	fmt.Println("The following env vars can be set:")
	for _, v := range utils.GetStructTags(Cfg, tagName) {
		fmt.Printf("%s\n", v)
	}
}

func main() {
	Logger.Infof("service started at %s", time.Now())

	// Load env vars from file
	if err := godotenv.Load(configFilename); err != nil {
		Logger.With("error", err).Warn("could not read configuration from file")
	}
	// Get config from env vars
	if err := env.Parse(&Cfg); err != nil {
		Logger.With("error", err).Fatal("could not parse configuration")
	}

	// Create server
	srv, err := NewWeepiAuth()
	if err != nil {
		Logger.With("error", err).Fatal("instanciate server error")
	}
	if err = srv.registerMsgActions(); err != nil {
		Logger.With("error", err).Fatal("could not register msg actions")
	}

	// External TLS
	opts := []grpc.ServerOption{
		grpc.UnaryInterceptor(srv.AuthInterceptor),
	}
	if Cfg.UseTLS {
		creds, err := credentials.NewServerTLSFromFile(Cfg.TLSCrt, Cfg.TLSKey)
		if err != nil {
			Logger.With("error", err).Fatal("could not set TLS")
		}
		opts = append(opts, grpc.Creds(creds))
	}

	gExt := grpc.NewServer(opts...)

	pb.RegisterWeepiAuthServer(gExt, srv)

	// ext
	extLis, err := net.Listen("tcp", Cfg.ListenAddr)
	if err != nil {
		Logger.With("error", err).Fatalf("listening on address %s error", Cfg.ListenAddr)
	}
	Logger.Infof("server started and listening on address %s", Cfg.ListenAddr)

	defer extLis.Close()

	if err = gExt.Serve(extLis); err != nil {
		Logger.With("error", err).Fatal("serving error")
	}
}
