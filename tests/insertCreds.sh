#!/bin/bash

echo "$(cat)" | polyglot \
	--command call \
	--use_reflection false \
	--proto_discovery_root $GOPATH/src/gitlab.com/weepi/auth/proto \
	--add_protoc_includes /usr/include/,$GOPATH/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
	--endpoint localhost:1666 \
	--use_tls true \
	--tls_ca_cert_path deploy/certs/ca/ca.crt \
	--tls_client_cert_path deploy/certs/client/weepi_auth.crt \
	--tls_client_key_path deploy/certs/client/weepi_auth.java.key \
	--full_method='.WeepiAuth/InsertCreds' \
	--metadata useruuid:00000000-0000-0000-0000-000000000000
