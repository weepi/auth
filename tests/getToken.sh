#!/bin/bash

echo "$(cat)" | polyglot \
	--command=call \
	--use_reflection=false \
	--proto_discovery_root=$GOPATH/src/gitlab.com/weepi/auth/proto \
	--add_protoc_includes=/usr/include/,$GOPATH/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
	--endpoint localhost:1665 \
	--full_method='.WeepiAuth/GetToken'
